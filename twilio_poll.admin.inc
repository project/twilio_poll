<?php

/**
 * Form callback for 'twilio-poll/create'.
 *
 * @param      $form
 * @param      $form_state
 *
 * @param null|\TwilioPoll $poll
 *
 * @return mixed
 */
function twilio_poll_edit_poll($form, &$form_state, $poll = NULL) {
  $has_votes = FALSE;
  $vote_edit_access = user_access('edit twilio sms polls with votes');
  if ($poll && $has_votes = twilio_poll_votes_get_by_poll($poll, FALSE)) {
    $form['live_editing_header'] = [
      '#type'   => 'fieldset',
      '#title' => t("Poll with votes"),
      '#tree' => FALSE,
    ];
    $message = <<<EOL
Note that this poll already has votes, therefore editing it might cause loss of incoming votes, alteration of results and
 other negative side effects. It's is highly discouraged to do so unless you know what you're doing.<br>
 <i>Note</i>: to edit a Poll with existing votes you also need the adequate permission level: !perm_name
EOL;
    $form['live_editing_header']['message'] = [
      '#prefix' => '<div class="messages warning">',
      '#suffix' => '</div>',
      '#markup' => t($message, ['!perm_name'=>'edit twilio sms polls with votes']),
    ];
    $form['live_editing_header']['live_editing_confirm'] = [
      '#type'          => 'checkbox',
      '#title'         => t("Confirm edit poll with votes."),
      '#description'   => t("By checking this box you confirm that you might encur in data loss of alteration of poll results."),
    ];
  }

  $input = $form_state['input'];
  $form['poll_sms_id'] = [
    '#type'          => 'textfield',
    '#title'         => t("Poll SMS identifier"),
    '#description'   => t(
      "This is the Poll identifier, which must be unique site-wide and will be the <strong>first word</strong> of the SMS."
    ),
    '#maxlength'     => 16,
    '#default_value' => $input['poll_sms_id'] ?? ($poll ? $poll->getSmsId() : ''),
    '#required'      => empty($poll),
    '#attributes' => [
//      'readonly' => $has_votes || ($has_votes && !$vote_edit_access),
    ],
  ];

  $form['poll_title'] = [
    '#type'          => 'textfield',
    '#title'         => t("Poll title"),
    '#description'   => t(
      "This is the header shown for the poll. Usually a question like <em>'Which of those do you prefer?'</em>"
    ),
    '#required' => TRUE,
    '#default_value' => $input['poll_title'] ?? ($poll ? $poll->getTitle() : ''),
    '#attributes' => [
//      'readonly' => $has_votes || ($has_votes && !$vote_edit_access),
    ],
  ];

  $form['poll_status'] = [
    '#type'          => 'checkbox',
    '#title'         => t("Currently accepting votes."),
    '#description'   => t("Indicates if the poll is open and able to receive votes or it's closed."),
    '#default_value' => $input['poll_status'] ?? ($poll ? $poll->isOpen() : TRUE),
  ];

  $form['poll_options'] = [
    '#type'   => 'fieldset',
    '#title' => t('Poll Options'),
    '#prefix' => '<div id="twilio-poll-options-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  ];
  // Set the minimum of 2 poll options, otherwise it makes no sense.
  if (empty($form_state['poll_options_count']) || $form_state['poll_options_count'] < 2) {
    $form_state['poll_options_count'] = 2;
    if ($poll) {
      $form_state['poll_options_count'] = max(2, count($poll->getOptions()));
    }
  }

  // Create the poll options fields
  $existing_poll_options = isset($poll) ? array_values($poll->getOptions()) : [];
  for ($i = 0; $i < $form_state['poll_options_count']; $i++) {
    // Easy access to the form input values.
    $values = [];
    if (!empty($input['poll_options'][$i])) {
      $values = $form_state['input']['poll_options'][$i];
    }
    else if(!empty($existing_poll_options[$i])) {
      $values = $existing_poll_options[$i]->toArray();
    }

    $option_id = $values['sms_id'] ?? ($i + 1);
    $form['poll_options'][$i] = [
      '#type'   => 'fieldset',
      '#collapsible' => TRUE,
      // Collapse if already filled.
      '#collapsed' => isset($values['sms_id']),
      '#title' => t('Option: <strong>@title</strong>', ['@title' => $option_id]),
      '#attributes' => [
//        'readonly' => $has_votes || ($has_votes && !$vote_edit_access),
      ],
    ];
    // Easy access to the fieldset.
    $fieldset = &$form['poll_options'][$i];

    if (!empty($values['deleted'])) {
      // Add a strike-through on the title
      $form['poll_options'][$i]['#title'] = '<s>' . $form['poll_options'][$i]['#title'] . '</s> &bull; ' . t("Deleted");
      $fieldset['sms_id'] = [
        '#type'          => 'hidden',
        '#default_value' => $values['sms_id'],
      ];
      $fieldset['deleted'] = [
        '#type'          => 'hidden',
        '#default_value' => TRUE,
      ];
    }
    else {
      $fieldset['sms_id'] = [
        '#type'          => 'textfield',
        '#title'         => t("Poll SMS identifier"),
        '#description'   => t(
          "This is the Option/Vote identifier, which will be the <strong>second word</strong> of the SMS."
        ),
        '#maxlength'     => 16,
        '#required'      => TRUE,
        '#default_value' => $values['sms_id'] ?? '',
        '#attributes' => [
//          'readonly' => $has_votes || ($has_votes && !$vote_edit_access),
        ],
      ];

      $fieldset['title'] = [
        '#type'          => 'textfield',
        '#title'         => t("Option text"),
        '#description'   => t(
          "This option title. Usually something like <em>'Cats'</em>, <em>'Dogs'</em> or <em>'Hamsters'</em>"
        ),
        '#default_value' => $values['title'] ?? '',
      ];

      if (!$has_votes) {
        $fieldset['remove_option'] = [
          '#type'   => 'submit',
          '#value'  => t('Remove this'),
          '#name'   => "remove_option_{$i}",
          '#submit' => ['twilio_poll_edit_poll_remove_option'],
          '#limit_validation_errors' => [],
          '#ajax'   => [
            'callback' => 'twilio_poll_edit_poll_options_callback',
            'wrapper'  => 'twilio-poll-options-wrapper',
          ],
        ];
      }
    }
  }

  $form['poll_options']['add_more'] = [
    '#type'   => 'submit',
    '#value'  => t('Add more'),
    '#submit' => ['twilio_poll_edit_poll_add_one_option'],
    '#limit_validation_errors' => [],
    '#ajax'   => [
      'callback' => 'twilio_poll_edit_poll_options_callback',
      'wrapper'  => 'twilio-poll-options-wrapper',
    ],
  ];

  $form['actions'] = [
    '#type'  => 'actions',
    '#title' => t('Actions'),
  ];

  $form['actions']['submit'] = [
    '#type'  => 'submit',
    '#value' => t("Save"),
  ];

  if ($poll) {
    $form_state['is_edit_form'] = TRUE;
  }

  return $form;
}


/**
 * Callback for "add_more" form twilio_poll_edit_poll form
 *
 * @param $form
 * @param $form_state
 */
function twilio_poll_edit_poll_add_one_option($form, &$form_state) {
  $form_state['poll_options_count']++;
  $form_state['rebuild'] = TRUE;
}

function twilio_poll_edit_poll_remove_option($form, &$form_state) {
  // Get the Index of the parent "poll_option" clicked element
  $delta_option = $form_state['clicked_button']['#array_parents'][1];
  $form_state['input']['poll_options'][$delta_option]['deleted'] = TRUE;

  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback for the add_more button
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function twilio_poll_edit_poll_options_callback($form, &$form_state) {
  return $form['poll_options'];
}

/**
 * Validation function for twilio_poll_edit_poll form.
 *
 * @param $form
 * @param $form_state
 *
 * @return bool
 */
function twilio_poll_edit_poll_validate($form, &$form_state) {
  $input = &$form_state['input'];

  // Force uppercase and check for existing polls
  if (empty($form_state['is_edit_form'])) {
    $input['poll_sms_id'] = strtoupper($input['poll_sms_id']);
    if (twilio_poll_load_poll_single($input['poll_sms_id'])) {
      form_set_error('poll_sms_id', t("Poll identifiers must be unique."));
    }
    else if (!_twilio_poll_is_valid_identifier($input['poll_sms_id'])) {
      form_set_error('poll_sms_id', t("Poll identifier must not contain any space and be maximum 16 characters."));
    }
  }


  // Check for duplicates in the options
  $existing_options = [];
  foreach ($input['poll_options'] as $i => &$option) {
    // Skip any validation for deleted options
    if (empty($option['deleted'])) {
      $option['sms_id'] = strtoupper($option['sms_id']);
      if (in_array($option['sms_id'], $existing_options)) {
        form_set_error("poll_options][{$i}][sms_id", t("Option identifiers must be unique!"));
      }
      else {
        $existing_options[] = $option['sms_id'];
      }
      if (!_twilio_poll_is_valid_identifier($option['sms_id'])) {
        form_set_error("poll_options][{$i}][sms_id", t("Option identifier must not contain any space and be maximum 16 characters."));
      }
    }
  }
}

/**
 * Submit function for twilio_poll_edit_poll form.
 *
 * @param $form
 * @param $form_state
 */
function twilio_poll_edit_poll_submit($form, &$form_state) {
  $input = &$form_state['input'];

  // Transaction helps preventing inconsistent data
  $transaction = db_transaction();
  try {
    // First update or store the poll basic info
    twilio_poll_store_poll($input['poll_sms_id'], $input['poll_title'], $input['poll_status']);

    // Then delete the Poll Options marked for deletion
    $removed_options = [];
    foreach ($input['poll_options'] as $delta => $poll_option) {
      if (!empty($poll_option['deleted'])) {
        $removed_options[] = $poll_option['sms_id'];
        $input['poll_options'][$delta] = FALSE;
      }
    }
    if (count($removed_options)) {
      twilio_poll_delete_poll_options($input['poll_sms_id'], $removed_options);
    }
    // Finally update or store the remaining options
    twilio_poll_store_poll_options($input['poll_sms_id'], $input['poll_options']);
  }
  catch (Exception $e) {
    // Rollback changes on DB write exceptions
    $transaction->rollback();
    watchdog_exception('twilio_poll', $e);
    drupal_set_message(t('Could not save the Poll. This error has been logged.'), 'error');
    form_set_error('poll_sms_id');
  }

  drupal_set_message("The poll has been  successfully saved.");
}


function twilio_poll_admin_table($form, &$form_state) {
  $header = [
    'id' => t("ID"),
    'poll_id' => t("SMS ID"),
    'title'   => t("Title"),
    'options' => t("Options"),
    'status' => t("Status"),

    'vote_count' => t("Vote Count"),

    'edit' => t("Operations"),
  ];

  $rows = [];
  foreach (twilio_poll_load_poll_multiple() as $poll_id => $twilio_poll) {
    $rows[$poll_id] = $twilio_poll->toArray();
    $rows[$poll_id]['options'] = count($rows[$poll_id]['options']);
    $rows[$poll_id]['vote_count'] = count(twilio_poll_votes_get_by_poll($twilio_poll));

    $edit_path = 'twilio-poll/' . $twilio_poll->getSmsId() . '/edit';
    $rows[$poll_id]['edit'] = l('Edit', $edit_path, ['query' => drupal_get_destination()]);
  }

  $form['table'] = [
    '#theme'      => 'table',
    '#caption'    => t('Twilio Polls'),
    '#header'     => $header,
    '#rows'       => $rows,
    '#empty'      => t('There is no poll in the system,click the following button to add one.'),
    '#attributes' => [
      'id' => 'twilio-sms-polls',
    ],
  ];

  $form['actions'] = [
    '#type'  => 'actions',
    '#title' => t('Actions'),
  ];

  $form['actions']['new'] = [
    '#type'  => 'link',
    '#title' => t("Create new"),
    '#href' => 'twilio-poll/create',
    '#options' => ['query' => drupal_get_destination()],
  ];

  return $form;
}
