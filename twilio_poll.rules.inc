<?php

/**
 * Implements hook_rules_data_info().
 */
function twilio_poll_rules_data_info() {
  return [
    'twilio_poll' => [
      'label'         => t('Twilio Poll'),
    ],
  ];
}

/**
 * Implements hook_rules_condition_info().
 */
function twilio_poll_rules_condition_info() {
  $conditions = [];

  $conditions['twilio_poll_rules_is_valid_sms_poll'] = [
    'label'     => t('Vote is for an existing open Poll'),
    'group'     => t('Twilio SMS Poll'),
    'parameter' => [
      'sms' => ['label' => t('SMS'), 'type' => 'sms'],
    ],
    'module'    => 'twilio_poll',
  ];

  $conditions['twilio_poll_rules_is_unique_vote'] = [
    'label'     => t('Vote is unique (for a Poll)'),
    'group'     => t('Twilio SMS Poll'),
    'parameter' => [
      'sms'         => ['label' => t('SMS'), 'type' => 'sms'],
    ],
    'module'    => 'twilio_poll',
  ];

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function twilio_poll_rules_action_info() {
  $actions = [];

  $actions['twilio_poll_rules_load_poll_from_sms'] = [
    'label'     => t('Load a Twilio Poll'),
    'group'     => t('Twilio SMS Poll'),
    'parameter' => [
      'string' => [
        'type'        => 'sms',
        'label'       => t('Source SMS'),
        'description' => t('SMS Containing the vote info'),
      ],
    ],
    'provides'  => [
      'twilio_poll' => [
        'type'  => 'twilio_poll',
        'label' => t("A Twilio Poll"),
      ],
    ],
  ];

  $actions['twilio_poll_rules_add_vote'] = [
    'label'     => t('Add a vote to a Poll'),
    'group'     => t('Twilio SMS Poll'),
    'parameter' => [
      'string'      => [
        'type'        => 'sms',
        'label'       => t('Source SMS'),
        'description' => t('SMS Containing the vote info'),
      ],
      'twilio_poll' => [
        'type'  => 'twilio_poll',
        'label' => t("A Twilio Poll"),
      ],
    ],
  ];

  return $actions;
}


/**
 * Callback for rules condition, validates a SMS vote.
 *
 * A casted SMS vote is only valid if *all* the following conditions are met:
 * - Vote can be parsed (therefore contains at the least 2 words)
 * - The 1st word is a "Keyword" which is specified in pollim->field_sms_keyword
 * - The 2nd word must be be the first word of *one of the possible choices* in the field_answers
 *
 * @param array $sms
 *
 * @return bool
 */
function twilio_poll_rules_is_valid_sms_poll($sms) {
  $number = $sms['number'] ?? '';
  $text = $sms['message'] ?? '';


  // Parse the text and attempt loading the vote.
  if ($text && $vote = TwilioSmsVote::fromSmsBody($text, $number)) {
    $poll = _twilio_poll_load_poll_by_sms($vote);

    // Poll must be loaded AND open for votes.
    if ($poll && $poll->isOpen()) {
      foreach ($poll->getOptions() as $poll_option) {
        if ($vote->getVote() === $poll_option->getSmsId()) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
}


/**
 * Check is a number has already voted.
 * @param $sms
 * @param $poll
 *
 * @return bool
 */
function twilio_poll_rules_is_unique_vote($sms) {
  $poll = NULL;
  $text = $sms['message'] ?? '';

  // Parse the text and attempt loading the vote.
  $sms_vote = isset($sms['message']) ? TwilioSmsVote::fromSmsBody($text, $sms['number'] ?? '') : NULL;
  if ($sms_vote && $poll = _twilio_poll_load_poll_by_sms($sms_vote)) {
    return empty(twilio_poll_votes_get_by_phone_number($poll, $sms_vote->getSource()));
  }

  return FALSE;
}

function twilio_poll_rules_load_poll_from_sms($sms) {
  $poll = NULL;
  $number = $sms['number'] ?? '';
  $text = $sms['message'] ?? '';

  // Parse the text and attempt loading the vote.
  if ($text && $sms_vote = TwilioSmsVote::fromSmsBody($text, $number)) {
    // If an entity is loaded consider the vote valid.
    $poll = _twilio_poll_load_poll_by_sms($sms_vote);
  }

  return [
    'twilio_poll' => $poll,
  ];
}

function twilio_poll_rules_add_vote($sms, $poll) {
  $number = $sms['number'] ?? '';
  $text = $sms['message'] ?? '';

  // Parse the text and attempt loading the vote.
  if ($text && $vote = TwilioSmsVote::fromSmsBody($text, $number)) {
    $vote = twilio_poll_cast_vote($poll, $vote);

    return TRUE;
  }

  return FALSE;
}
