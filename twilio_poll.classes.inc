<?php

class TwilioPoll {
  /** @var int */
  protected $id;
  /** @var string */
  protected $sms_id;
  /** @var string */
  protected $title;
  /** @var bool */
  protected $status;
  /** @var TwilioPollOption[] */
  protected $options;

  public function __construct(int $id = NULL, string $sms_id, string $title, $status, array $options = []) {
    $this->id = $id;
    $this->sms_id = $sms_id;
    $this->title = $title;
    $this->status = (bool) $status;
    // Only allow TwilioPollOption to be added.
    $this->options = array_filter(
      $options,
      function ($item) {
        return is_a($item, TwilioPollOption::class);
      }
    );
  }

  /**
   * @return string
   */
  public function getId(): int {
    return $this->id ?? 0;
  }

  /**
   * @return string
   */
  public function getSmsId(): string {
    return $this->sms_id;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return bool
   */
  public function isOpen(): bool {
    return $this->status;
  }

  /**
   * @return \TwilioPollOption[]
   */
  public function getOptions(): array {
    return $this->options;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'id'      => $this->id,
      'poll_id' => $this->sms_id,
      'title'   => $this->title,
      'options' => $this->options,
      'status' => $this->status,
    ];
  }
}

class TwilioPollOption {
  /** @var int */
  protected $id;
  /** @var string */
  protected $poll_id;
  /** @var string */
  protected $title;
  /** @var string  */
  protected $sms_id;

  public function __construct(int $id = NULL, string $sms_id, string $poll_id, string $title) {
    $this->id = $id;
    $this->sms_id = $sms_id;
    $this->poll_id = $poll_id;
    $this->title = $title;
  }

  /**
   * @return string
   */
  public function getId(): int {
    return $this->id ?? 0;
  }

  /**
   * @return string
   */
  public function getSmsId(): string {
    return $this->sms_id;
  }

  /**
   * @return string
   */
  public function getPollId(): string {
    return $this->poll_id;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'id'      => $this->id,
      'sms_id'  => $this->sms_id,
      'poll_id' => $this->poll_id,
      'title'   => $this->title,
    ];
  }
}


/**
 * Auxiliary class to handle Votes
 */
class TwilioSmsVote {
  protected $source;
  protected $words_list;

  /**
   * @param string $body
   *
   * @return \TwilioSmsVote|null
   */
  public static function fromSmsBody(string $body, string $source = ''): ?self {
    // Uppercase for standardization
    $body = strtoupper($body);
    // Break on spaces.
    $words_list = preg_split('/\s+/', trim($body), -1, PREG_SPLIT_NO_EMPTY);

    if (count($words_list) >= 2) {
      $obj = new static();
      $obj->words_list = $words_list;
      $obj->source = $source;

      return $obj;
    }

    return NULL;
  }

  /**
   * Returns the keyword for the poll
   *
   * @return string
   */
  public function getKeyword(): string {
    return $this->words_list[0];
  }

  /**
   * Returns the Vote identifier for the poll
   *
   * @return string
   */
  public function getVote(): string {
    return $this->words_list[1];
  }

  public function setSource(string $source) {
    $this->source = $source;
  }

  public function getSource(): string {
    return $this->source ?? '';
  }
}
