<div class="poll-answers-wrapper">
    <div class="poll-answers">
      <?php foreach ($answers as $answer): ?>
        <?php if (!empty($answer['poll_option'])): ?>
          <div class="vote-bar" data-percent="<?= $answer['vote_percent'] ?>" data-count="<?= $answer['vote_count'] ?>" data-sms-id="<?= $answer['poll_option']->getSmsId() ?>">
              <span class="answer-sms-id"><?= $answer['poll_option']->getSmsId() ?></span><span class="answer-title"><?= $answer['poll_option']->getTitle() ?></span>
              <div class="percentage-bar">
                  <div class="bar" style="width: <?= $answer['vote_percent'] ?>%;"></div>
              </div>
          </div>
        <?php endif;?>
      <?php endforeach; ?>
    </div>
    <div class="votes-total"><span class="count"><?= t('Votes count: @count', ['@count' => $votes_count]) ?></span></div>
</div>
