<?php

function theme_twilio_poll(&$variables) {
  $twilio_number = variable_get('twilio_number', 'N/A');
  $poll_vars = [
    'title' => '',
    'poll' => $variables['poll'],
    'poll_answers'     => [],
    'votes_count' => 0,
    'sms_number'  => $twilio_number,
  ];

  /** @var \TwilioPoll $poll */
  if ($poll = $variables['poll']) {
    $poll_answers = $poll->getOptions();
    $poll_results = twilio_poll_votes_get_by_poll($poll);

    if ($variables['show_title']) {
      $poll_vars['title'] = $poll->getTitle();
    }

    $poll_vars['vote_instructions'] = t(
      'Send <span class="poll-sms-id">@sms_id [OPTION]</span> to <a href="@sms_uri">@phone_number</a>.<br>Available options are: <span class="poll-sms-id id=list">@answers</span>',
      [
        '@sms_id'       => $poll->getSmsId(),
        '@phone_number' => $twilio_number,
        '@sms_uri'      => "sms://{$twilio_number}",
        '@answers' => implode(', ', array_map(
          function (TwilioPollOption $item) { return $item->getSmsId(); },
          $poll_answers
        )),
      ]
    );

    /** @var \TwilioPollOption $poll_option */
    foreach ($poll_answers as $poll_option) {
      $poll_vars['poll_answers'][$poll_option->getId()] = [
        'poll_option' => $poll_option,
        'vote_percent' => 0,
        'vote_count' => 0,
      ];
    }

    foreach ($poll_results as $poll_result) {
      $match = [];
      // Check if a "value" is either a percent or a count
      if (preg_match('/^(?:option\-)(?<id>\d+)(?<is_percent>\-percent)?$/', $poll_result['function'], $match)) {
        $answer_id = $match['id'];
        if (isset($match['is_percent'])) {
          $poll_vars['poll_answers'][$answer_id]['vote_percent'] = $poll_result['value'];
        }
        else {
          $poll_vars['poll_answers'][$answer_id]['vote_count'] = $poll_result['value'];
        }
      }
      elseif ($poll_result['function'] === 'total') {
        // Special treatment for the Totals.
        $poll_vars['votes_count'] = $poll_result['value'];
      }
    }

    // drupal_add_js(drupal_get_path('module', 'twilio_poll') . '/twilio_poll.js', ['every_page' => FALSE]);
    drupal_add_css(drupal_get_path('module', 'twilio_poll') . '/twilio_poll.css', ['every_page' => FALSE]);
  }

  return theme('twilio_poll_item', $poll_vars);
}
