<?php if ($poll && is_a($poll, TwilioPoll::class)): ?>
<div class="twilio-poll <?= "poll-" . $poll->getId() . " poll-sms-id-" . $poll->getSmsId(); ?>">
    <div class="heading">
        <?php if ($title): ?><h3 class="poll-title"><span class="poll-sms-id"><?= $poll->getSmsId() ?></span><?= $title ?></h3><?php endif; ?>
        <p class="vote-instructions"><?= $vote_instructions ?></p>
    </div>
    <?= theme('poll_result_answers', ['answers' => $poll_answers, 'votes_count' => $votes_count, 'sms_number' => $sms_number]); ?>
</div>
<?php endif; ?>
